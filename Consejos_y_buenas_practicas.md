# Consejos y buenas practicas

## Herramientas

El lenguaje de programación Java puede ser desarrollado de con muchas herramientas,
pero si no se quiere batallar lo mejor es usar un IDE (Integrated Development
Environment) como lo son, por ejemplo:

* [Netbeans](https://netbeans.org)
* [Eclipse](https://www.eclipse.org/ide/)

Aprendan a utilizar un sistema de control de versiones y un servicio de
repositorios para mantener su codigo, puede que ahora les parezca algo
inutil pero les servira muchisimo mas adelante.

* [Git](https://git-scm.com/)
* [Gitlab](https://gitlab.com/)
* [Github](https://github.com/)

## Código

Traten de usar un estilo de escritura a lo largo de todos sus codigos, esto
ayuda a que sea mas facil de leer y de revisar mas adelante, algunos de estilos
de escritura son:

* [Camel Case](https://en.wikipedia.org/wiki/Camel_case)
* [Snake case](https://en.wikipedia.org/wiki/Snake_case)

Procuren siempre identar sus codigos para mejor legibilidad, puede que un
**"for"** pueda ponerse en una sola linea pero no siempre es lo mejor para
observar su funcionamiento.